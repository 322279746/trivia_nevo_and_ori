#include "RecievedMassege.h"

RecievedMessages::RecievedMessages(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

RecievedMessages::RecievedMessages(SOCKET sock, int messageCode, vector<string> values) : RecievedMessages(sock, messageCode)
{
	_values = values;
}

SOCKET RecievedMessages::getSock()
{
	return _sock;
}


int RecievedMessages::getMessageCode()
{
	return _messageCode;
}


vector<string>& RecievedMessages::getValues()
{
	return _values;
}
