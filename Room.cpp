#include "Room.h"
#include <string>
#include "Protocol.h"


Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionsNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}

string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	int start = 0;
	const int _ZERO = 0;
	string UsersString;

	vector<User*>::iterator it = usersList.begin();

	for (it = it; it != usersList.end(); ++it)
	{
		if (((usersList[start]->getUserName()).compare((excludeUser[start].getUserName())) != _ZERO))
		{
			UsersString.append((usersList[start]->getUserName()));
		}
	}

	return(UsersString);
}


string Room::getUsersListMessage()
{
	int start = 0, UserSize = _users.size();
	string viewList = SEND_USER_LIST, all, sharp = "##";
	const int _ZERO = 0;

	all = std::to_string(UserSize);
	viewList.append(all);

	for (start = _ZERO; start < UserSize; start++)
	{
		viewList.append(sharp);
		viewList.append(_users[start]->getUserName());
	}

	return(viewList);
}

void Room::sendMessage(User* excludeUser, string message)
{
	int place = 0, UserSize = _users.size();
	const int _ZERO = 0;

	for (place = _ZERO; place < UserSize; place++)
	{
		if (((excludeUser->getUserName()).compare(((_users[place])->getUserName())) != _ZERO))
		{
			_users[place]->Send(message);
		}
	}

}

void Room::sendMessage(string message)
{
	sendMessage(NULL, message);
}

bool Room:: JoinRoom(User* user)
{
	int UserSize = _users.size(), place = 0;
	string myNewUsers = getUsersListMessage();
	const int _ZERO = 0;
	bool bol = true;

	if (UserSize >= _maxUsers)
	{
		bol = false;
		user->Send(FAILD_JOIN_EXSIST_ROOM_ANSWER);
	}
	else
	{
		_users.push_back(user);

		for (place = _ZERO ; place < UserSize; place++)
		{
			_users[place]->Send(myNewUsers);
			user->Send(JOIN_EXSIST_ROOM_ANSWER);
		}
		
	}

	return bol;
}


void Room::LeaveRoom(User* user)
{
	int place = 0, UserSize = _users.size();
	const int _ZERO = 0;
	string myNewUsers = getUsersListMessage();


	for (place = _ZERO; place < UserSize; place++)
	{
		if (((user->getUserName()).compare(((_users[place])->getUserName())) == _ZERO))		
		{
			_users.erase(_users.begin() + place);
			UserSize--;
			user->Send(LEAVE_ROOM_ANSWER_SUCCESS);
		}
	}

	for (place = _ZERO; place < UserSize; place++)
	{
		_users[place]->Send(myNewUsers);
	}

}


int Room::closeRoom(User* user)
{
	int ret = _id;
	const int _ZERO = 0;

	int place = 0 ,UserSize = _users.size();

	if (((user->getUserName()).compare((_admin->getUserName())) == _ZERO))
	{
		for (place = _ZERO; place < UserSize && ((((_users[place])->getUserName() ).compare((_admin->getUserName())) != _ZERO)); place++)
		{
			_users[place]->Send(CLOSE_ROOM_ANSWER);
			_users[place]->ClearRoom();
		}
	}
	else
	{
		ret = -1;	
	}

	return(ret);
}


 













