#pragma once
#define _WINSOCKAPI_ 

#include "Helper.h"
#include <string>
#include <iostream>
#include <mutex>
#include <thread>		
#include <string>
#include <vector>
#include "Room.h"
#include "Game.h"
#include <Windows.h>


using namespace std;

class Room;
class Game;

class User
{
public:
	User(string username, SOCKET sock);
	void Send(string message);
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime);
	string getUserName();
	bool joinRoom(Room* newRoom);
	void LeaveRoom();
	int CloseRoom();
	void ClearRoom();

private:
	Game* _currGame;
	Room* _currRoom = nullptr;
	string  _UserName;
	SOCKET _sock;

};

