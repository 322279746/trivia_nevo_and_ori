#pragma once

#include <vector>
#include <string>
#include "User.h"

using namespace std;

class User;

class Room
{
public:
	Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime);
	string getUsersListMessage();
	bool JoinRoom(User* user);
	void LeaveRoom(User* user);
	int closeRoom(User* user);


private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	
	void sendMessage(User* excludeUser, string message);
	void sendMessage(string message);
	string getUsersAsString(vector<User*> usersList, User* excludeUser);
	
};

 
