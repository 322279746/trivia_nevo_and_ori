#include "Helper.h"
#include <string>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>		
#include <queue>
#include "User.h"
#include "RecievedMassege.h"
#include <condition_variable>

using namespace std;


class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void srever();
	

private:
	SOCKET _socket;
	std::map<SOCKET, User*> _connectedUsers;
	std::map<int, Room* > _roomList;
	int static _roomldSeqeunce;
	std::queue<RecievedMessages*> _messageHandler;
	std::mutex _mtxRecievedMessages;
	std::condition_variable _msgQueueCondition;
	std::condition_variable _edited;
	
	void bindAndListen();
	void acceptClient();
	void clientHandler(SOCKET client_socket);
	void handleRecievedMessages();
	//void safeDeleteUser(RecievedMessages* recM);


	//User* handleSignin(RecievedMessages* recM);
	//bool handleSignup(RecievedMessages* recM);
	//void handleSignout(RecievedMessages* recM);
	//void handleLeaveGame(RecievedMessages* recM);
	//void handleStartGame(RecievedMessages* recM);
	//void handlePlayerAnswer(RecievedMessages* recM);
	//bool handleCreatRoom(RecievedMessages* recM);
	//bool handleCloseRoom(RecievedMessages* recM);
	//bool handleJoinRoom(RecievedMessages* recM);
	
	void addRecievedMessage(RecievedMessages* recM);
	RecievedMessages* buildRecieveMessage(SOCKET client_socket, int msgCode);

};
