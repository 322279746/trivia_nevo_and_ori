#include "TriviaServer.h"
#include <exception>
#include "RecievedMassege.h"
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <deque>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <iomanip>
#include <sstream>
#include "Helper.h"


static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;



TriviaServer::TriviaServer()
{
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
		
}

TriviaServer::~TriviaServer()
{	
	try
	{
		::closesocket(_socket);
	}

	catch (...) {}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;

	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}


	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}

}

void TriviaServer::srever()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		acceptClient();
	}
}


RecievedMessages* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessages* msg = nullptr;
	vector<string> values;
	if (msgCode == MT_CLIENT_LOG_IN)
	{
		int userSize = Helper::getIntPartFromSocket(client_socket, 2);
		string userName = Helper::getStringPartFromSocket(client_socket, userSize);
		values.push_back(userName);
	}
	else if (msgCode == MT_CLIENT_FINISH || msgCode == MT_CLIENT_UPDATE)
	{
		int fileSize = Helper::getIntPartFromSocket(client_socket, 5);
		string fileContent = Helper::getStringPartFromSocket(client_socket, fileSize);
		values.push_back(fileContent);

	}

	msg = new RecievedMessages(client_socket, msgCode, values);
	return msg;

}

void TriviaServer::clientHandler(SOCKET client_socket)
{
	RecievedMessages* currRcvMsg = nullptr;

	try
	{
		
		int msgCode = Helper::getMessageTypeCode(client_socket);

		while (msgCode != 0 && (msgCode == MT_CLIENT_LOG_IN || msgCode == MT_CLIENT_UPDATE || msgCode == MT_CLIENT_FINISH))
		{
			currRcvMsg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(client_socket);
		}

		currRcvMsg = buildRecieveMessage(client_socket, MT_CLIENT_EXIT);
		addRecievedMessage(currRcvMsg);

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		currRcvMsg = buildRecieveMessage(client_socket, MT_CLIENT_EXIT);
		addRecievedMessage(currRcvMsg);
	}

	closesocket(client_socket);
}

void TriviaServer::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
	{ 
		throw std::exception(__FUNCTION__);
	}
		
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::addRecievedMessage(RecievedMessages* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_messageHandler.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}

void TriviaServer::handleRecievedMessages()
{
	
}

