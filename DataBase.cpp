#include "DataBase.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;
int callback(void *data, int argc, char **argv, char **azColName);
static int callbackQ(void* notUsed, int argc, char **argv, char **azColName);

DataBase::DataBase()
{
	int rc;
	char *zErrMsg = 0;

	rc = sqlite3_open("OurData.db", &db);

	rc = sqlite3_exec(db, "CREATE table t_users(UserName VARCHAR PRIMARY KEY NOT NULL, Password VARCHAR NOT NULL, email VARCHAR NOT NULL)", 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}


	rc = sqlite3_exec(db, "CREATE table t_games(GameId integer NOT NULL PRIMARY KEY autoincrement, status integer NOT NULL, Start_time DATETIME NOT NULL, End_time DATETIME)", 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}

	

	rc = sqlite3_exec(db, "CREATE table t_questions(questionsId integer PRIMARY KEY NOT NULL autoincrement,question VARCHAR NOT NULL, correct_ans VARCHAR NOT NULL, ans2 VARCHAR NOT NULL ,ans3 VARCHAR NOT NULL ,ans4 VARCHAR NOT NULL )", 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}

	rc = sqlite3_exec(db, "PRAGMA foreign_keys=1", 0, 0, &zErrMsg);
	rc = sqlite3_exec(db, "CREATE table t_players_answers(foreign key(GameId integer PRIMARY KEY NOT NULL) REFERENCES t_games(GameId) ,foreign key(username VARCHAR NOT NULL PRIMARY KEY) REFERENCES t_users(UserName), foreign key(questions_id  VARCHAR NOT NULL PRIMARY KEY) REFERENCES t_questions(questionsId), player_answer VARCHAR NOT NULL, is_correct integer NOT NULL ,answer_time integer NOT NULL)", 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}


}

DataBase::~DataBase()
{
	sqlite3_close(db);
}

bool DataBase::isUserExists(string userName)
{
	int rc = 0;
	char* thePass;
	char* ErrorMsg = 0;
	const int _ZERO = 0;
	const char *uname = userName.c_str();
	bool ret = false;
	string message = "SELECT Password FROM t_users WHERE UserName=" + userName;

	rc = sqlite3_exec(db, message.c_str(), callback, &thePass, &ErrorMsg);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	if (strcmp(thePass, NULL) != _ZERO)
	{
		ret = true;
	}

	return(ret);
}

bool DataBase::addNewUser(string username, string password, string email)
{
	int rc = 0;
	char *ErrorMsg = 0;
	bool ret = true;

	string message = "INSERT INTO t_users(UserName) values('" + username + "')";

	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc != SQLITE_OK)
	{
		ret = false;
	}

	message = "INSERT INTO t_users(Password) values('" + password + "')";

	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc != SQLITE_OK)
	{
		ret = false;
	}

	message = "INSERT INTO t_users(email) values('" + email + "')";

	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc != SQLITE_OK)
	{
		ret = false;
	}

	return(ret);
} 

bool DataBase::isUserAndPassMatch(string userName, string password)
{
	int rc = 0;
	char* thePass;
	char* ErrorMsg = 0;
	const int _ZERO = 0;
	const char *uname = userName.c_str();
	bool ret = false;
	string mes = "SELECT Password FROM t_users WHERE UserName=" + userName;


	rc = sqlite3_exec(db, mes.c_str(), callback, &thePass, &ErrorMsg);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		return(1);
	}

	uname = password.c_str();

	if (strcmp(uname, thePass) == _ZERO)
	{
		ret = true;
	}

	

	return(ret);
}

vector<Question*> DataBase:: initQuestions(int questionsNo)
{
	int rc = 0;
	string message, number = to_string(questionsNo);
	char* ErrorMsg = 0; 
	vector<Question*> sendVec;

	for (int i = 0; i < questionsNo ; i++)
	{
		message = "SELECT *  from t_questions order by RANDOM() LIMIT 1";
	
		rc = sqlite3_exec(db, message.c_str(), callbackQ, &sendVec, &ErrorMsg);

		if (rc)
		{
			cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
			sqlite3_close(db);
		}
	
	}


	return(sendVec);
}

int DataBase::insertNewGame()
{
	int rc = 0;
	char* ErrorMsg = 0;
	string message; 
	

	message = "INSERT INTO t_games(status) values(0)";
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	message = "INSERT INTO t_games(Start_time) values(DateTime.Now)";
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc)
	{
		cout << endl << "sorry I can't open your file..." << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
	}

	return(0);
}

bool DataBase:: updateGameStatus(int gameId)
{
	int rc = 0;
	char* ErrorMsg = 0;
	string message;
	bool ret = true;


	message = "UPDATE t_games SET status = 1 WHERE GameId = " + gameId;
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	message = "UPDATE t_games SET End_time = DateTime.Now WHERE GameId = " + gameId;
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc)
	{
		ret = false;
	}

	return(ret);
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	int rc = 0, insert = 0;
	char* ErrorMsg = 0;
	string message;
	bool ret = true;


	message = "INSERT INTO t_players_answers(GameId) values(" + gameId + ')';
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	message = "INSERT INTO t_players_answers(UserName) values('" + username + "')";
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	message = "INSERT INTO t_players_answers(question_id) values(" + questionId + ')';
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	message = "INSERT INTO t_players_answers(answer) values('" + answer + "')";
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (isCorrect)
	{
		insert = 1;
	}

	message = "INSERT INTO t_players_answers(is_correct) values(" + insert + ')';
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);
	  
	message = "INSERT INTO t_players_answers(answer_time) values(" + answerTime + ')';
	rc = sqlite3_exec(db, message.c_str(), NULL, 0, &ErrorMsg);

	if (rc)
	{
		ret = false;
	}

	return(ret);
	
}

int callback(void *data, int argc, char **argv, char **azColName)
{
	string value;

	*static_cast<int*>(data) = atoi(argv[0]);

	return(0);
}	

static int callbackQ(void* notUsed, int argc, char **argv, char **azColName)
{
	vector<Question*>* rec = (vector<Question*>*)notUsed;
	Question* currQuest;

	currQuest = new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]);
	rec->push_back(currQuest);

	return 0;
}
