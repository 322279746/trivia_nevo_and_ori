#include "Game.h"
#include "Protocol.h"


Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) :_db(db)
{
	_players = players;
	_questionsNo = questionsNo;
	
	_db.insertNewGame();

}

Game::~Game()
{
	_questions.clear();
}

void Game:: sendQuestionToAllUsers()
{
	int Aplayers = _players.size();
	const int _ZERO = 0;
	int place ;
	string mass;
	string* answer = (_questions[0]->getAnswers());
	mass = "118###" + (_questions[0]->getQuestion()) + "###" + answer[0] + "###" + answer[1] + "###" + answer[2] + "###" + answer[3];


	for (place = _ZERO; place < Aplayers; place++)
	{
		_players[place]->Send(mass);

	}

}

void Game::handleFinishGame()
{
	_db.updateGameStatus(getID());
}

int Game::getID()
{
	return(0);
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

bool Game::handleNextTurn()
{
	int Aplayers = _players.size(), Aquestion = _questions.size();
	bool ret = false;

	if (_players.empty())
	{
		handleFinishGame();

	}

	else
	{
		if (_currentTurnAnswers == Aplayers)
		{
			if (_questionsNo == Aquestion)
			{
				handleFinishGame();
			}

			else
			{
				_questionsNo++;
				sendQuestionToAllUsers();
			}
		}
		ret = true;
	}
		
	return(ret);
}

bool Game::handleAnswerFromUser(User* user, int AnswerNumber, int Time)
{
	string userN = user->getUserName();
	bool ret = false;
	_currentTurnAnswers++;

	if (AnswerNumber == _questions[0]->getCorrectAnswerIndex())
	{
		std::map<string, int>::iterator it = _result.find(userN);

		if (it != _result.end())
		{
			it->second = (it->second)++;

		}
	}

	return (ret);
}

bool Game::leaveGame(User* currUser)
{
	int place = 0, Aplayers = _players.size();
	const int _ZERO = 0;

	for (place = _ZERO; place < Aplayers; place++)
	{
		if ((currUser->getUserName()).compare((_players[place]->getUserName())) == _ZERO)
		{
			_players.erase(_players.begin() + place);
			handleNextTurn();
		}

	}
}
