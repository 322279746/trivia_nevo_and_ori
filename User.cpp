#include "User.h"
#include "Helper.h"



User::User(string username, SOCKET sock) :_sock(sock)
{
	_UserName = username;
}

string User::getUserName()
{
	return(_UserName);
}

void User::Send(string message)
{
	Helper myLittleHelp;
	
	myLittleHelp.sendData(_sock, message);
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	bool ret = false;

	if (_currRoom != NULL)
	{
		Send("1141");
	}

	else
	{
		_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		Send("1140");
		ret = true;
	}

	return(ret);
}


bool User::joinRoom(Room* newRoom)
{
	bool ret = true;

	if (_currRoom != nullptr)
	{
		newRoom->JoinRoom(this);
	}

	else
	{
		ret = false;
	}

	return(ret);
}

void User::LeaveRoom()
{
	bool ret = true;

	if (_currRoom != nullptr)
	{
		_currRoom->LeaveRoom(this);
		_currRoom = nullptr;
	}

}

int User::CloseRoom()
{ 
	int ret = 0;
	
	if (_currRoom != nullptr)
	{
		ret = (_currRoom->closeRoom(this));
	}

	else
	{
		ret = -1;
	}

	return(ret);
}


void User::ClearRoom()
{
	_currRoom = nullptr;
}