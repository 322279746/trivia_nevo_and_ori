#include "Question.h"
#include <time.h>       

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	int theRandNum, theRandNum1, theRandNum2, theRandNum3;
	
	_id = id;
	_question = question;
	
	srand(time(NULL));

	theRandNum = rand() % 4 + 1;
	_answers[theRandNum] = correctAnswer;
	_correctAnswerIndex = theRandNum;
	
	do
	{
		theRandNum1 = rand() % 4 + 1;
	} while (theRandNum1 == theRandNum);
	
	_answers[theRandNum1] = answer2;

	do
	{
		theRandNum2 = rand() % 4 + 1;
	} while (theRandNum2 == theRandNum || theRandNum2 == theRandNum1);

	_answers[theRandNum2] = answer3;

	do
	{
		theRandNum3 = rand() % 4 + 1;
	} while (theRandNum3 == theRandNum || theRandNum3 == theRandNum1 || theRandNum3 == theRandNum2);

	_answers[theRandNum3] = answer4;

}

string Question::getQuestion()
{
	return(_question);
}

string* Question::getAnswers()
{
	return(_answers);
}

int Question::getCorrectAnswerIndex()
{
	return(_correctAnswerIndex);
}


int Question::getId()
{
	return(_id);
}

