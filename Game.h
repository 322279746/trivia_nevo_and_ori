#pragma once

#include "Question.h"
#include "User.h"
#include "DataBase.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>


using namespace std;
class Question;
class DataBase;
class User;

class Game
{
public:
	
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int AnswerNumber, int Time);
	int getID();
	bool leaveGame(User* currUser);

private:

	void sendQuestionToAllUsers();

	std::vector<Question*> _questions;
	std::vector<User*> _players;
	
	DataBase& _db;
	map < string , int > _result;
	
	int  _questionsNo;
	int _currQuestionIndex;
	int _currentTurnAnswers;


};