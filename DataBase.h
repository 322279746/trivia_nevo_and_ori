#pragma once

#include "sqlite3.h"
#include <vector>
#include "Question.h"

class Question;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string userName);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<Question*> initQuestions(int questionsNo);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);

private:
	sqlite3* db;
};